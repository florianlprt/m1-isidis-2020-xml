<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Country</th>
        <th>Continent</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>...</td>
        <td>...</td>
        <td>...</td>
      </tr>
    </tbody>
  </table>
</xsl:template>

</xsl:stylesheet>
